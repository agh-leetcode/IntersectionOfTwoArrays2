import java.util.*;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int nums1 [] = new int[]{3,1,2};
        int nums2 [] = new int[]{1,3};
        int nums3 [] = new int[]{4,9,5};
        int nums4 [] = new int[]{9,4,9,8,4};
       // intersect(nums3,nums4);
        intersection(nums3,nums4 );
    }

    private int[]intersection(int [] nums1, int[]nums2){
        List<Integer> ints = new ArrayList<>();
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int i = 0;
        int j = 0;
        while (i<nums1.length && j < nums2.length){
            if (nums1[i]<nums2[j])
                i++;
            else if (nums1[i]>nums2[j])
                j++;
            else
            {
                ints.add(nums2[j++]);
                i++;
            }
        }
        int result [] = new int[ints.size()];
        for (int i1 = 0; i1 < result.length; i1++) {
            result[i1]=ints.get(i1);
        }
        for (int i2 : result) {
            System.out.println(i2);
        }
        return result;
    }
}
